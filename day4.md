[[TOC]]

# GitOps to Automate the Setup, Management and Extension a K8s Cluster

_Kim Schlesinger, DigitalOcean_

 * https://sched.co/yto4

## Description

In this workshop, you will experience the power of Infrastructure as Code and GitOps to automate the provisioning, 
modification, and extension of a Kubernetes cluster. Join me to learn how to use Terraform to spin up a Kubernetes 
cluster and install FluxCD, which will watch a GitHub repo and automatically apply any changes made via git commit. 
In order to keep all of your credentials like secrets, passwords, and tokens in your GitHub repo, we will show you 
how to use the sealed-secrets project to enable one-way encrypted secrets that can only be decoded inside the cluster. 
Finally, you will install and use Crossplane to provision digital infrastructure from inside your Kubernetes cluster, 
including resources from different cloud providers, giving you a chance to experiment with multi-cloud infrastructure.

## Notes

 * The talk was around a specific command line tool for digital ocean explicitly, so not sure if it is of value for us.¨
 * https://github.com/digitalocean/kubecon-2022-doks-workshop
 * Live demo with 5 steps.
 * Requirements
   * Digital Ocean Account
   * Github Account
 * doctl generate token
 * doctl -> digital ocean cli

# OpenTelemetry: The Vision, Reality, and How to Get Started

_Dotan Horovits, Logz.io_

 * https://sched.co/ytob

## Description

Everyone wants observability into their system, but find themselves with too many vendors and tools, each with its own 
API, SDK, agents and collectors. In this talk Horovits will present OpenTelemetry, an ambitious open source project 
with the promise of a unified framework for collecting observability data. With OpenTelemetry you could instrument 
your application in a vendor-agnostic way, and then analyze the telemetry data in your backend tool of choice, whether 
Prometheus, Jaeger, Zipkin, or others. Horovits will cover the current state of the various projects comprising 
OpenTelemetry (across programming languages, exporters, receivers, protocols and more), some of which are not even 
GA yet, and provide practical guidance on how to get started with OpenTelemetry in your own system.

## Notes

 * 5-10 different tools in general for projects to collect telemetry data.
 * Logz.io -> Observability Specialists.
 * ![](images/20220519_120014.jpg)
 * Merge of Opensensus and OpenTrace
 * Second most active project in CNCF after kubernetes by contribution.
 * Industry is aligning behind opentelemetry.
 * ![](images/20220519_120815.jpg)
 * OTLP -> Open Telemetry Language Protocol
 * Trace is GA (Generally Available) for a year.
 * Metric is GA in weeks, RC for python.
 * Prometheus support
 * Logs - Expermental
 * ![](images/20220519_121857.jpg)
 * ![](images/20220519_122244.jpg)

# Reproducing Production Issues in your CI Pipeline Using eBPF

_Matthew LeRay, Speedscale & Omid Azizi, New Relic_

 * https://sched.co/ytpE

## Description

Observing production workloads with enough detail to find real problems is difficult, but it's getting easier with the 
community adoption of eBPF. As the technology becomes better understood, tools like Falco, Cilium and Pixie are 
increasingly appearing in production clusters. But have you ever considered using eBPF data to help with unit tests, 
Continuous Integration and load testing? This talk will explain the basic technology behind eBPF while presenting 
some examples of how to use data collected via eBPF for a variety of software quality use cases. We'll use the Pixie 
CNCF sandbox project to pull data and replicate production issues on the developer desktop for debugging. You'll 
also get some ideas on using those calls in your Continuous Integration pipeline to sanity check builds before they 
are deployed. Included in that discussion will be handling some common issues like timestamp skew and authentication. 
All examples are open source and available after the talk.

## Notes

 * Traffic replay to increase test.
 * ![](images/20220519_143216.jpg)
 * Traffic replay with persistent storage.
 * github.com/speedscale/demo
 * github.com/speedscale/pixie_to_curl
 * px.dev

# Cilium: Welcome, Vision and Updates

_Thomas Graf & Liz Rice, Isovalent; Laurent Bernaille, Datadog_

 * https://sched.co/ytq0

## Description

If you’re interested in using Cilium, or contributing to the project, this session is for you. Our agenda for this 
session: 1. Introduction to Cilium A brief overview of the origin and vision for Cilium. 2. Working with Cilium An 
end user's perspective of using Cilium. 3. Cilium Service Mesh Cilium can be used as a highly efficient service mesh 
data plane. Let’s discuss the learnings from our beta, and the upcoming roadmap. We will leave time for Q&A, and an 
opportunity to meet Cilium maintainers and contributors.

## Notes

 * eBPF makes the kernel programmable
 * Cilium
   * CNI
   * Service Mesh
   * Hubble
 * Service Mesh
 * ![](images/20220519_153307.jpg)
 * Tetragon is now ope source
 * ![](images/20220519_153345.jpg)
 * ![](images/20220519_154929.jpg)
 * Slackchannel is preferred way of introduction.
 * Post in specific channel for response, not general.
 * Community project.

# Interesting Talks that I did not attend:

 * [Kubernetes IoT Edge Working Group: Edge Device Onboarding and Management](https://sched.co/ytoP)
 * [Better Reliability Through Observability and Experimentation](https://sched.co/ytp5)
 * [How to Migrate 700 Kubernetes Clusters to Cluster API with Zero Downtime](https://sched.co/yttp)
 * [The Future Of Reproducible Research: Powered By Kubeflow](https://sched.co/ytpB)
 * [Emissary + Linkerd: A Guide to End-to-end Encryption for your Cluster](https://sched.co/11LW4)
 * [Securing Your Container Native Supply Chain with SLSA, Github and Tekton](https://sched.co/ytq9)
 * [Jet Energy Corrections with GNN Regression using Kubeflow at CERN](https://sched.co/ytqv)
 * [Service Mesh at Scale: How Xbox Cloud Gaming Secures 22k Pods with Linkerd](https://sched.co/ytuJ)
