[[TOC]]

# A Treasure Map of Hacking (and Defending) Kubernetes

_Andrew Martin, ControlPlane_

 * https://sched.co/ytre

## Description

In this ultimate guide to threat-driven defence, we threat model Kubernetes and detail how to attack and defend your 
precious clusters from nefarious adversaries. This broad and detailed appraisal of end-to-end cluster security teaches 
you how to defend against a range of historical and current CVEs, misconfigurations, and advanced attacks: - See the 
historical relevance of CVEs and demonstrations of attacks against your containers, pods, supply chain, network, 
storage, policy, and wider organisation - Understand when to use next-generation runtimes like gVisor, firecracker, 
and Kata Containers - Delve into workload identity and advanced runtime hardening - Consider the trust boundaries in 
soft- and hard-multitenant systems to appraise and limit the effects of compromise - Learn to navigate the choppy 
waters of advanced Kubernetes security.

## Notes

 * Controlplane homesite has a free book.
 * Thread modelling
 * ![](images/20220520_110501.jpg)
 * ssh, gpg, aws, gke keys is available on your computer.
 * npm install executes precommit hooks to inject malicious code.
 * Coming into the pod gives fill access to node if you run as root.
 * @KrisNova -> Security Expert.
 * Reverse Shell is one of the most used attacks.
 * Kernel Vulnerability - Not solved by host.
 * Access tpo AWS keys gives access to the AWS cluster.
 * Canarytoken identifies stolen tokens.

# Logs Told Us It Was DNS, It Felt Like DNS, It Had To Be DNS, It Wasn’t DNS

_Laurent Bernaille & Elijah Andrews, Datadog_

 * https://sched.co/ytrw

## Description

It all started with a team reaching out because they had DNS issues during rolling updates. Business as usual when you 
host hundreds of applications on dozens of Kubernetes clusters… Four weeks later: We are reading kernel code to 
understand the corner cases of dropping Martian packets. Could this be the connection between gRPC client reconnect 
algorithms and the overflowing conntrack table we can feel but not see? In time, we solved the issue. And for once… 
it wasn't DNS! In this talk, we will focus on one of the most complex incidents we have faced in our Kubernetes 
environment. We will go through the debugging steps in detail, dive deep into the mysterious behaviors we discovered 
and explain how we finally addressed the incident by simply removing three lines of code.

## Notes

 * Running on all major cloud vendors.
 * ![](images/20220520_115954.jpg)
   * Route53
   * AWS Resolved
 * Node Local DNS (SVLD??) was running out of memory.
 * NLD can't create connections?
 * We hit max concurrent 1000 with only 200 rps.
 * ENA (Elastic Network Adapter) metrics -> Amazon
 * AWS Networking
   * Are we bursting over the instance limits?
   * Conntrack allowance exceeded?
   * Connection tracking is required.
 * UPC log flows.
 * Egress flows by source.
 * Ingress should ~match Egress.
 * Reconnect attemps 130k over 90 seconds.
 * ![](images/20220520_121316.jpg)
 * Alerting Engine is SYN flooding the metrics client
 * ![](images/20220520_121532.jpg)
 * Invalid Cross-Device link
 * Cilium Issue 18505 pull request.
 * Tell cilium to not do send as response.
 * ![](images/20220520_122117.jpg)
 * gRPC client configuration
 * DNS propagation time during rollouts.
 * External dns updated wen the pos was actually deleted instead of when the pod gor the sigkill.

# Kubernetes Everywhere: Lessons Learned From Going Multi-Cloud

_Niko Smeds, Grafana Labs_

 * https://sched.co/ytsi

## Description

Many companies are interested in deploying their products across multiple cloud providers, but few actually see it 
through. While benefits like avoiding provider lock-in and increased uptime during provider outages are attractive, 
several factors are important to consider. Grafana Labs successfully deployed across AWS, Azure, DigitalOcean, GCP, 
and Linode in more than 30 regions; that includes inter-cloud network connections. This talk will explore some of the 
large and subtle differences in networking and managed Kubernetes services between said providers. We’ll discuss the 
approaches we took while scaling our infrastructure across multiple environments, the challenges we faced, and what 
worked in the end.

## Notes

 * Lessons learned going multicloud
 * Increase available regions.
 * Reduce vendor lockin
 * on GCP and move to AWS.
 * Cross-Procider VPN:s
 * Managed Kubernetes. Rely on the experts of GCP and AWS.
 * Flux Continuous Deployment.
 * IP Range Overload.
 * Be ready to fail and start over.
 * Use IaC and Git.
 * Don't blindly do what was done before.
 * Do unified better solutions. Don't crowbar it into existing solutions.
 * Poor disk performance on aws by defalt.
 * Docker hub rate limits as AWS don't have docker cache like GCP.
 * Quotas is present in new cloud
   * know your quotas
 * Monitor and alert on quotas.

# Throw Away Your Passwords: Trusting Workload Identity

_Ric Featherstone, ControlPlane_

 * https://sched.co/ytsx

## Description

Trust is required to secure our systems: we need it to bootstrap infrastructure, to run workloads, and to reassure our 
customers of their privacy. But how do we establish and secure this "trust" in a dynamic cloud native system?

Historically we relied upon identifiers such as IP addresses, passwords, and certificates, but can we do better than 
these antiquated authentication mechanisms? In this talk we:
 * Demystify machine identity and its relationship to secrets management and access control
 * Discuss the issues with historical approaches in a cloud native environment
 * Solve the "bottom turtle" trust bootstrap quandary
 * Appraise the open source implementations and technologies available to you
 * Demonstrate practical examples of how to acquire a workload identity or secret zero
 * Strive for a world in which passwords and static keys are replaced by dynamic credentials and hardware roots of trust

## Notes

 * Kerberos machine trust between machines.
 * Dunamic infrastructure
 * Secret Zero
   * Secret to access other secrets.
 * Don't use long lived service account tokens.
   * Not rotated
 * Service account issues 1.20 in kubernetes.
 * Verify id of token locally so you don't need to verify against vault.
 * JWT (Java Web Token)
   * <HEADER>.<PAYLOAD>.<SIGNATURE>
 * Trust claims in token.
 * Supported by:
   * GKE
   * AWS
   * Azure
 * Can be used with anything provided from GKE
 * Can use non long lived credentials for pipelines.
 * Will automatically mutat pods with the api keys.
 * OPA to enforce policy control (Open Policy Agent)
 * Your identity provider foes not have to be the cloud provider.
   * It can be github for example
 * SPIFFE defines the standards for the identity framework.
 * SPIRE is the runtime of SPIFFE
 * SPIFFE consist of JWT or X.509 certificates.
 * ![](images/20220520_152005.jpg)
 * Envoy can be used to secure with TLS.
 * Workload Identities.
 * http://keylime.dev
   * Uses linux IMA
 * ![](images/20220520_152712.jpg)

# Interesting Talks that I did not attend:

 * [From Cloud Naive to Cloud Native – Avoiding Mistakes Everyone Does](https://sched.co/ytrY)
 * [SIG Security Update: We Lift Together](https://sched.co/ytrn)
 * [Staring Into the Abyss with the Security Technical Advisory Group](https://sched.co/ytrq)
 * [Threat Hunting at Scale: Auditing Thousands of Clusters With Falco + Fluent](https://sched.co/ytrP)
 * [Writing Crossplane Providers with Code Generation](https://sched.co/ytrS)
 * [What Anime Taught Me About K8s Development & Tech Careers](https://sched.co/yts8)
 * [Show Me Your Labels and I’ll Tell You Who You Are](https://sched.co/ytrz)
 * [GitOpsify Everything: When Crossplane Meets Argo CD](https://sched.co/yts5)
 * [Full Mesh Encryption in Kubernetes with WireGuard and Calico](https://sched.co/ytrt)
 * [Better Bandwidth Management with eBPF](https://sched.co/ytsQ)
 * [Linkerd End User Panel: Case Studies from Production](https://sched.co/ytsZ)
 * [Simplifying Service Mesh Operations with Flux and Flagger](https://sched.co/ytt9)
 * [From Monitoring to Observability: Left Shift your SLOs with Chaos](https://sched.co/yttd)
 * [Making Sense of Chaos: Implementing Chaos Engineering in a Fintech Company](https://sched.co/ytuP)
