# Day 1 - Cloud Native Security Con

 * Schedule - https://cloudnativesecurityconeu22.sched.com/
 * PDF Notes - [CNSC Day 1](files/CNSC_day1.pdf) 
 * Details - [Talks Day 1](day1.md)

# Day 2 - Cloud Native Security Con

 * Schedule - https://cloudnativesecurityconeu22.sched.com/
 * PDF Notes - [CNSC Day 2](files/CNSC_day2.pdf) 
 * Details - [Talks Day 2](day2.md)

# Day 3 - CNCF Kubernetes Day 1

 * Schedule - https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/program/schedule/
 * PDF Notes - [CNSF Day 3](files/CNCF_day3.pdf) 
 * Details - [Talks Day 3](day3.md)

# Day 4 - CNCF Kubernetes Day 2

 * Schedule - https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/program/schedule/
 * PDF Notes - [CNSF Day 4](files/CNCF_day4.pdf) 
 * Details - [Talks Day 4](day4.md)

# Day 5 - CNCF Kubernetes Day 3

 * Schedule - https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/program/schedule/
 * PDF Notes - [CNSF Day 5](files/CNCF_day5.pdf) 
 * Details - [Talks Day 5](day5.md)


