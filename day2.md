[[TOC]]

# Vanquishing Vulnerabilities in Valencia

_Alba Ferri Fitó, Sysdig & Eric Smalling, Snyk_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUk/vanquishing-vulnerabilities-in-valencia-alba-ferri-fito-sysdig-eric-smalling-synk

## Description

The infamous Log4Shell vulnerability took us all by surprise right as we were preparing to take our end-of-year 
vacations! Will there be another massive vulnerability to deal with this year? It’s very possible, but you can be ready 
for it! Join us to learn how you can prepare your organization for the next critical CVE and make it harder for 
attackers to leverage it against you. From the developers’ shell to runtime in production, there are many tools and 
practices you can put in place today that can mitigate and detect would-be attackers and make their lives harder. 
Topics will include container image construction and scanning, policy enforcement, controlling network traffic, safer 
runtime configurations, and monitoring runtime behavior. This session will include live demonstrations of the log4shell 
remote code exploit and how effective the techniques presented can be against attacks on it.

## Notes

 * Most docker images defaults to root user.
 * Minimize footprint of containers.
   * Avoid big toolkit for people that gets into the pods.
 * ![](images/20220517_095045.jpg)
 * ![](images/20220517_095113.jpg)
 * Immutable makes things harder.
 * Network policy is a string security towards log4j / log4shell
 * Deny all ingress / egress on solutions and only add what is needed. (Least Privilege)
 * OPA / Kyerno / PSA
 * _Shift security left_ (more security resolving on the devs, using SAST, DAST and security champions)
 * _Shield security right_ (to runtime)
 * See normal behaviour of workloads.
 * Falco detects non normal behaviour on workloads.
   * Filter and apply rules.
   * Send alerts in realtime.
   * Use simple, common language rules.
 * Falco plugins can detect uncertain behaviour.
 * Runtime intelligence.

# Shrinking Software Attack Surface with WebAssembly & CNCF Wasmcloud

_Liam Randall, Cosmonic_

 * https://cloudnativesecurityconeu22.sched.com/event/zsU4/shrinking-software-attack-surface-with-webassembly-cncf-wasmcloud-liam-randall-cosmonic

## Description

WebAssembly is poised to fundamentally transform the development of both browser and server-side development. The 
virtualization of the CPU, OS, and cloud with hypervisor, containers, and Kubernetes each marked epochs of technology 
that ushered in emerging trends in software architecture, design, development, operation, and life cycle management. 
In this session, we highlight the development and advantages of WebAssembly and the CNCF wasmCloud Application 
Framework. WebAssembly marks the next wave of cloud-native evolution. In this demonstration heavy session, we 
highlight 3 main advantages driving the adoption of these technologies focusing on the security impacts: 1. With 
WebAssembly's virtualization of the application, we demonstrate portability across diverse CPUs, clouds, Kubernetes 
distributions, edges, and web browsers. 2. Through a capability-driven sandbox we demonstrate a security model that 
is sandboxed, portable, and consistent across the diverse execution environments. 3. With wasmClouds actor model we 
demonstrate a streamlined approach to managing the software supply chain by virtualizing the use of non-functional 
requirements and common open source libraries.

## Notes

 * Liam Randell @ Github
 * Alpine -> Scratch docker size.
 * Assume we run on linux
 * Assume we have secure OS.
 * ![](images/20220517_103530.jpg)
 * Write once run anywhere (WASM/Webassembly), like java and all other things you should be sceptical.
 * WASM is a compile target so most languages can be used.
 * Webassembly - Virtual CPU.
 * Abstract components like:
   * SQL
   * Message Queue
   * Hotswappable capabilities.
 * User edge computing
   * WASM Cloud.

# Lighting Talk: Lessons Learned from Writing Thousands of Lines of IaC

_Eran Bibi, Firefly_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUS/lighting-talk-lessons-learned-from-writing-thousands-of-lines-of-iac-eran-bibi-firefly

## Description

Immutable architecture is the backbone of infrastructure as code & cloud native operations, to ensure production 
environments cannot be changed during runtime. While this has the benefits of its inherent safety measures, this can 
also be restrictive, all while creating new challenges for security. Immutable concepts are much more effective when 
it comes to securing cloud native environments and infrastructure, which is becoming an increasingly more complex task.
This talk will focus on some of the fundamentals of immutable architecture, best practices and recommended design 
patterns to work around its limitations and enhance security, as well as what you most certainly should not be doing 
when running immutable architecture both from an infrastructure and security perspective. This will be demonstrated 
through a real-world example of deploying a single-tenant SaaS in an automated pipeline, typical challenges 
encountered, and what was learned on the way, through a Terraform, Kubernetes and step functions example.

## Notes

 * IAC -> Infrastructure as Code
 * State Files is accessed from all third party resources.
 * Do not blindly trust public modules.
 * ![](images/20220517_110614.jpg)

# Lightning Talk: Repurposed Purpose: Using Git's DAG for Supply Chain Artifact Resolution

_Aeva Black, Microsoft_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUV/lightning-talk-repurposed-purpose-using-gits-dag-for-supply-chain-artifact-resolution-aeva-black-microsoft

## Description

What if we could know the complete and reproducible artifact tree for every binary executable, shared object, 
container, &etc – including all its dependencies – and you could efficiently cross-reference that against a database 
of known vulnerabilities? If you had had that information, could you have remediated Log4Shell faster? Might it even 
help open source maintainers identify at-risk dependencies sooner? If you're thinking, "this sounds too good to be 
true - what's it going to cost?", then we really hope you’ll join us because we believe this should be an automatic 
part of open source build tools. In this talk, Aeva and Ed will share why they're so excited about GitBOM and explain
what it is (hint: it's not git and it's not an SBOM). If the demo gods are willing, they will show you how you can 
generate a GitBOM with a simple command-line tool, and explain why you won't have to.

## Notes

 * @AevaBlack
 * ![](images/20220517_111450.jpg)
 * ![](images/20220517_111726.jpg)
 * Git Bom
 * ![](images/20220517_111903.jpg)
 * ![](images/20220517_111920.jpg)
 * ![](images/20220517_112034.jpg)

# Top 5 Reasons (and 5 Myths Debunked) to Invest in Securing the Software Supply Chain

_Hector Linares, Microsoft_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUh/top-5-reasons-and-5-myths-debunked-to-invest-in-securing-the-software-supply-chain-hector-linares-microsoft

## Description

The recent Log4j vulnerability and NOBELIUM attack stress the importance of securing the software supply chain across 
the lifecycle: design, development, compilation, packaging, deployment, and maintenance. Executive Order 14028 mandates 
"significant investments" to help protect against malicious cyber threats and emphasizes a renewed focus on "enhancing 
software supply chain security," including compliance with the NIST Secure Software Development Framework (SSDF). To 
meet requirements of SSDF, we present a practitioner's guide for the journey ahead employing the Supply Chain Integrity 
Model (SCIM), an open-source model for managing data about the security, quality, and integrity of assets across 
end-to-end supply chains. We show how to maximize ROI in software supply chain security, enabling a trusted platform 
for the Software Development Lifecycle (SDLC) that extends to partners and customers.

## Notes

 * Estimated cost of 6 trillion $ in 2021.
 * Development -> Deployment -> Runtime
 * ![](images/20220517_150521.jpg)
 * SBOM -> Ingredient list
 * You cannot verify external SBOMs.
 * ![](images/20220517_151731.jpg)
 * https://github.com/lockc-project/lockc
 * Enforces policys on clusters like:
   * Not run as root.

# Lightning Talk: What Have We Learned from Scanning Over 10K Unique Clusters with Kubescape?

_Shauli Rozen, ARMO_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUq/lightning-talk-what-have-we-learned-from-scanning-over-10k-unique-clusters-with-kubescape-shauli-rozen-armo

## Description

Kubescape is a K8s open-source tool providing a multi-cloud K8s single pane of glass, including risk analysis, security 
compliance, RBAC visualizer and image vulnerabilities scanning. Kubescape scans K8s clusters, YAML files, and HELM 
charts, detecting misconfigurations according to multiple frameworks (such as the NSA-CISA, MITRE ATT&CK®), software 
vulnerabilities, and RBAC (role-based-access-control) violations at early stages of the CI/CD pipeline, calculates risk 
score instantly and shows risk trends over time. In the last 6 months, Kubescape scanned over 10K unique clusters and 
we learned a great deal about the state of Kubernetes risk, compliance, and security vulnerabilities. In this session, 
Shauli Rozen, ARMO CEO &Co-Founder, will share interesting insight on why and where Kubernetes deployments are failing, 
the weak spots, and how to get better. He will share some interesting statistics on which controls fail most and where 
and what are measures to take in order to prevent them.

## Notes

 * Cluster is generally increasing.
 * 25% of the users are devops people.
 * [NSA kubernetes best practices](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/2716980/nsa-cisa-release-kubernetes-hardening-guidance/) 
   security tools like [Mitre](https://www.mitre.org/capabilities/cybersecurity/overview?category=all).
 * ![](images/20220517_155730.jpg)
 * 37% of clusters had applications with credentials in the configuration files.
 * 25% of clusters had applications running with unsecure linux capabilities.
 * 35% had a critical vulnerability running in them.
 * kubescape on [github](https://github.com/armosec/kubescape)

# Real Time Security - eBPF for Preventing attacks

_Liz Rice, Isovalent_

 * https://cloudnativesecurityconeu22.sched.com/event/zsUt/real-time-security-ebpf-for-preventing-attacks-liz-rice-isovalent

## Description

eBPF is used in several cloud native security tools. In some respects it is already being used for preventative 
security: - Cilium uses eBPF to enforce NetworkPolicy - Default seccomp profiles - more properly called seccomp-bpf - 
limit the system calls that applications can use When it comes to runtime security, Falco today uses eBPF to detect 
suspicious application behavior, but this isn’t preventative - it generates alerts that are used asynchronously to 
react to malicious events. Is this really the best we can do with eBPF? The answer is a resounding “no”. In this talk 
we’ll dive into demos and code to explore how eBPF can be used for the next generation of security enforcement tooling. 
This talk will cover: - Why enforcing NetworkPolicy with eBPF has been in place for years, but preventative security 
for applications has taken longer - How Phantom attacks can compromise the use of basic system call hooks - How other 
eBPF attachment points, such as BPF LSM, can be used for preventative security You don’t need to know about eBPF to 
get the most out of this talk, but you will need a basic understanding of kernel and user space, and a willingness to 
see some C code

## Notes

 * Real time detection and prevention on the cluster.
 * LD_PRELOAD
   * Standard C Library
   * System call API
   * Replace the "standard" library
 * Syscall checks
   * ptrace
   * seccomp
   * eBPF probes on syscall entry.
 * Phantom Attacks
 * LSM - Linux Security Models
 * eBPF makes it dynamic@
 * Protect pre-existing processes.
 * eBPF is getting info on everything happening on that VM / Machine / Docker image.
 * Needs kernel 5.7+ to use the eBPF option.
 * eBPF can hook into any part of the kernel.
 * Cilium Tetragon is open sourced.
 * Using multiple co-ordinated eBPF programs.
 * in_kernel event filtering
 * Tetragon will notify you on all programs enter and exits.
 * What is eBPF
 * ![](images/20220517_163323.jpg)
 * Secure Observability with eBPF
 * cilium.io





