[[TOC]]

# Intro to Kubernetes, GitOps, and Observability Hands-On Tutorial

_Joaquin Rodriguez, Microsoft & Tiffany Wang, Weaveworks_

 * https://sched.co/ytkj

## Description 

This tutorial offers newcomers a quick way to experience Kubernetes and its natural evolutionary developments: GitOps 
and Observability. Attendees will be able to use and experience the benefits of Kubernetes that impact reliability, 
velocity, security, and more. The session will cover key concepts and practices, as well as offer attendees a way to 
experience the commands in real-time. The tutorial covers: - kubectl - K9s - Metrics (Prometheus) - Dashboards 
(Grafana) - Logging (Fluent Bit) - GitOps (FluxCD) Attendees will be able to walk through the steps via a browser-
based platform. Instructors will lead the topics and help to troubleshoot. Prerequisites: A computer with a modern 
browser (Edge, Chrome, Safari, Firefox) GitHub ID with 2FA enabled.

## Notes

 * [Flux](https://fluxcd.io/)
 * [opengitops.dev](https://opengitops.dev/)
 * Prometheus -> Grafana -> FluentBit

# Building Digital Twins for DFDS With Crossplane and Kubernetes

_Tobias Andersen, DFDS & Matthias Luebken, Upbound_

 * https://sched.co/ytle

## Description

Constant movement and adaptation to change is the story of DFDS, a logistics company from Denmark. Every ship, truck, 
or warehouse needs to be fully connected and be able to drive autonomous and smart decisions. At the same time, 
customer and business demands change constantly, and software development teams need to build and update solutions at 
an ever-increasing rate. In this talk, Tobias and Matthias like to introduce DFDS’s decentralized and distributed 
problem space and how they have mastered this challenge by introducing Kubernetes based Digital Twins. They will talk 
about how to align software teams from all over the organization, and how a platform build on top of Crossplane and 
Backstage can guide and accelerate teams.

## Notes

 * Digital representation of a physical asset
 * Controlling machinery
 * ![](images/20220518_144414.jpg)

# OpenTelemetry: The Road Ahead + Meet the Community

_Morgan McLean, Splunk; Alolita Sharma, Amazon; Ted Young, Lightstep; Daniel Dyla, Dynatrace_

 * https://sched.co/ytns

## Description

This session is for anyone interested in observability or OpenTelemetry to learn more about the project, and to meet 
and discuss our status and roadmap with maintainers! We will begin with a brief presentation of our recent releases 
and roadmap for the next year, followed by a panel discussion hosted by governance committee members, technical 
committee members, and maintainers.

## Notes

 * Metrics general availability from 18/5-2022
 * Profiling in interesting to see what functions take time.
 * Integrate Elastic Common Schema
 * Auto Instrumentation
 * Experience
   * Docs
   * Usability
 * Live Config Change
 * Load and Performance Testing.

# Notes

Day 3 had alot of interesting talks that collided or was full once I arrived, here is the links:

 * [Effective Disaster Recovery: The Day We Deleted Production](https://sched.co/ytlM)
 * [Unlimited Data Science Libraries, One Container Image, No Installation!](https://sched.co/ytlJ)
 * [Improving GPU Utilization using Kubernetes](https://sched.co/ytlt)
 * [Jaeger: Present and Future](https://sched.co/ytmf)
 * [Prow! Leveraging Developer-Centric CI for Your OSS Project!](https://sched.co/ytn6)
 * [Open Policy Agent (OPA) Intro & Deep Dive](https://sched.co/ytnI)
 * [Automated Progressive Delivery Using GitOps and Service Mesh](https://sched.co/ytng)
